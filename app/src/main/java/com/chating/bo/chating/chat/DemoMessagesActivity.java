package com.chating.bo.chating.chat;

import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;

import com.chating.bo.chating.Enity.Message;

import com.chating.bo.chating.Enity.User;
import com.chating.bo.chating.Chat_Fragment;
import com.chating.bo.chating.R;

import com.chating.bo.chating.Session;
import com.chating.bo.chating.chat.Data.Add_Dataset;
import com.chating.bo.chating.utils.AppUtils;
import com.chating.bo.chating.utils.FormatUtils;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.JsonElement;
import com.squareup.picasso.Picasso;
import com.stfalcon.chatkit.commons.ImageLoader;
import com.stfalcon.chatkit.messages.MessagesListAdapter;


import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.Map;
import java.util.UUID;


import ai.api.model.AIResponse;
import ai.api.model.Result;

/*
 * Created by troy379 on 04.04.17.
 */
public abstract class DemoMessagesActivity extends AppCompatActivity
        implements MessagesListAdapter.SelectionListener,
        MessagesListAdapter.OnLoadMoreListener {

    private static final int TOTAL_MESSAGES_COUNT = 100;

    protected final String senderId = "0";
    protected ImageLoader imageLoader;
    protected MessagesListAdapter<Message> messagesAdapter;

    private Menu menu;
    private int selectionCount;
    private Date lastLoadedDate;

    private DatabaseReference MessRef, UserRef;
    private String lastid="";

    MediaPlayer mediaPlayer;

    ////api.ai

    private    Add_Dataset dataset = new Add_Dataset();

    //// sent Infor
private String name, avatar;




    ArrayList<Message> messages ;

    /// session
    private Session session;



    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        /// session
        
        session = new Session(this);
        
        
        
        
        imageLoader = new ImageLoader() {
            @Override
            public void loadImage(ImageView imageView, String url) {
                Picasso.with(DemoMessagesActivity.this).load(url).into(imageView);


            }
        };

        
        //////



        mediaPlayer=MediaPlayer.create(this,R.raw.new_message);

        get_lastid();
        getInfor();
        get_Last();

    }





    @Override
    protected void onStart() {
        super.onStart();




    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        this.menu = menu;
        getMenuInflater().inflate(R.menu.chat_actions_menu, menu);
        onSelectionChanged(0);


        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_delete:
                messagesAdapter.deleteSelectedMessages();
                break;
            case R.id.action_copy:
                messagesAdapter.copySelectedMessagesText(this, getMessageStringFormatter(), true);
                AppUtils.showToast(this, R.string.copied_message, true);
                break;
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        if (selectionCount == 0) {
            super.onBackPressed();
        } else {
            messagesAdapter.unselectAllItems();
        }


    }

    @Override
    public void onLoadMore(int page, int totalItemsCount) {
        if (totalItemsCount < messages.size()) {
            loadMessages();
        }
    }

    @Override
    public void onSelectionChanged(int count) {
        this.selectionCount = count;
        menu.findItem(R.id.action_delete).setVisible(count > 0);
        menu.findItem(R.id.action_copy).setVisible(count > 0);
    }

    protected void loadMessages() {
if(messages.size()==0){

}
else {
    lastLoadedDate = messages.get(messages.size() - 1).getCreatedAt();
    messagesAdapter.addToEnd(messages, true);
}

    }


    private MessagesListAdapter.Formatter<Message> getMessageStringFormatter() {
        return new MessagesListAdapter.Formatter<Message>() {
            @Override
            public String format(Message message) {
                String createdAt = new SimpleDateFormat("MMM d, EEE 'at' h:mm a", Locale.getDefault())
                        .format(message.getCreatedAt());

                String text = message.getText();
                if (text == null) text = "[attachment]";

                return String.format(Locale.getDefault(), "%s: %s (%s)",
                        message.getUser().getName(), text, createdAt);
            }
        };
    }



    private void getInfor(){
        UserRef = FirebaseDatabase.getInstance().getReference().child("User").child(session.get_chat_to()).getRef();
        UserRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
               name = dataSnapshot.child("name").getValue().toString();
                avatar = dataSnapshot.child("avatar").getValue().toString();
                get_all_messager();
            }


            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }


   public void onResult(final AIResponse response) {
        Result result = response.getResult();

        // Get parameters
        String parameterString = "";
        if (result.getParameters() != null && !result.getParameters().isEmpty()) {
            for (final Map.Entry<String, JsonElement> entry : result.getParameters().entrySet()) {
                parameterString += "(" + entry.getKey() + ", " + entry.getValue() + ") ";
            }
        }


       sent_message_Firebase_bot(parameterString,"text");


    }
    private void sent_message_Firebase_bot(String contend, String type){

        User user = new User("1","bot","https://marketplace.canva.com/MAB6v9eTAHs/1/thumbnail/canva-robot-avatar-MAB6v9eTAHs.png",false);
        String uniqueID = UUID.randomUUID().toString(); //// unique id
        // Message message  =new Message(uniqueID,user,input.toString());
        Add_Dataset addmess  = new Add_Dataset();
        addmess.Add_Message(contend,user, session.get_chatID(),uniqueID,type);
    }

    private void get_all_messager(){
        MessRef = FirebaseDatabase.getInstance().getReference().child("Conversations").child(session.get_chatID()).getRef();

        messages = new ArrayList<Message>();

        ValueEventListener MessListener = new ValueEventListener() {
            @Override
            public void onDataChange(final DataSnapshot dataSnapshot) {


                for(final DataSnapshot singleSnapshot : dataSnapshot.getChildren()) {


                            Map singleUser = (Map) singleSnapshot.getValue();


                            Date date = FormatUtils.getDate((Long) singleUser.get("timestamp"));



                    dataset.readed(session.get_chatID(),singleSnapshot.getKey().toString());


                    if(singleUser.get("type").toString().equals("text")) {

                        if (singleUser.get("fromid").toString().equals(FirebaseAuth.getInstance().getCurrentUser().getUid())) {


                            User user_sent = new User("0", name, avatar, false);
                            Message dialog = new Message(singleSnapshot.getKey().toString(), user_sent, singleUser.get("content").toString(), date);
                            messages.add(dialog);

                        } else {
                            User user_sent = new User("1", name, avatar, false);
                            Message dialog = new Message(singleSnapshot.getKey().toString(), user_sent, singleUser.get("content").toString(), date);
                            messages.add(dialog);

                        }

                    }
                  else if(singleUser.get("type").toString().equals("image")){

                        if (singleUser.get("fromid").toString().equals(FirebaseAuth.getInstance().getCurrentUser().getUid())) {
                            User user_sent = new User("0", name, avatar, false);
                            Message dialog = new Message(singleSnapshot.getKey().toString(), user_sent, null, date);
                            dialog.setImage(new Message.Image(singleUser.get("content").toString()));
                            messages.add(dialog);

                        } else {
                            User user_sent = new User("1", name, avatar, false);
                            Message dialog = new Message(singleSnapshot.getKey().toString(), user_sent, null, date);
                            dialog.setImage(new Message.Image(singleUser.get("content").toString()));
                            messages.add(dialog);

                        }
                    }
                    else if(singleUser.get("type").toString().equals("location")){

                        if (singleUser.get("fromid").toString().equals(FirebaseAuth.getInstance().getCurrentUser().getUid())) {
                            User user_sent = new User("0", name, avatar, false);
                            Message dialog = new Message(singleSnapshot.getKey().toString(), user_sent, null, date);
                            String content = singleUser.get("content").toString() ;
                            String[] Place = content.split(":");

                            String Place_Url =  "http://maps.google.com/maps/api/staticmap?center=" +  Place[0].toString() + "," +  Place[1].toString() + "&zoom=15&size=600x400&sensor=false";

                            dialog.setImage(new Message.Image(Place_Url));
                            messages.add(dialog);

                        } else {
                            User user_sent = new User("1", name, avatar, false);
                            Message dialog = new Message(singleSnapshot.getKey().toString(), user_sent, null, date);
                            String content = singleUser.get("content").toString() ;
                            String[] Place = content.split(":");
                            String Place_Url =  "http://maps.google.com/maps/api/staticmap?center=" +  Place[0].toString() + "," +  Place[1].toString() + "&zoom=15&size=200x200&sensor=false";
                            dialog.setImage(new Message.Image(Place_Url));
                            messages.add(dialog);

                        }
                    }



                            //  Date date = new Date();




                }
                //    getmessge();
                loadMessages();

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        };
        MessRef.orderByChild("timestamp").addListenerForSingleValueEvent(MessListener);

    }
    private void get_lastid(){
        MessRef = FirebaseDatabase.getInstance().getReference().child("Conversations").child(session.get_chatID()).getRef();


        messages = new ArrayList<Message>();

        ValueEventListener MessListener = new ValueEventListener() {
            @Override
            public void onDataChange(final DataSnapshot dataSnapshot)
            {
                for(final DataSnapshot singleSnapshot : dataSnapshot.getChildren()) {
                    lastid = singleSnapshot.getKey().toString();

                }


            }

            @Override
            public void onCancelled(DatabaseError databaseError) {


            }
        };
        MessRef.orderByChild("timestamp").limitToLast(1).addListenerForSingleValueEvent(MessListener);

    }


    private void get_Last(){
        MessRef = FirebaseDatabase.getInstance().getReference().child("Conversations").child(session.get_chatID()).getRef();
        UserRef = FirebaseDatabase.getInstance().getReference().child("User").getRef();

        ChildEventListener event = new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                System.out.println(s);

                messages = new ArrayList<Message>();
                final DataSnapshot singleSnapshot = dataSnapshot;
              //  for(final DataSnapshot singleSnapshot : dataSnapshot.getChildren()) {

                    UserRef.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshotcon) {


                            DataSnapshot con = dataSnapshotcon.child(singleSnapshot.child("fromid").getValue().toString());
                            Date date = FormatUtils.getDate((Long) singleSnapshot.child("timestamp").getValue());
                            if (lastid.equals(singleSnapshot.getKey()) == false) {
                                lastid = singleSnapshot.getKey().toString();

                                if (singleSnapshot.child("type").getValue().toString().equals("text")) {


                                    if (singleSnapshot.child("fromid").getValue().toString().equals(FirebaseAuth.getInstance().getCurrentUser().getUid())) {


                                        User user_sent = new User("0", con.child("name").getValue().toString(), con.child("avatar").getValue().toString(), Boolean.valueOf((Boolean) con.child("online").getValue()));
                                        Message dialog = new Message(singleSnapshot.getKey().toString(), user_sent, singleSnapshot.child("content").getValue().toString(), date);


                                      //  mediaPlayer.start();

                                        messagesAdapter.addToStart(dialog, true);
                                        //  messages.add(dialog);
                                    } else {
                                        User user_sent = new User("1", con.child("name").getValue().toString(), con.child("avatar").getValue().toString(), Boolean.valueOf((Boolean) con.child("online").getValue()));
                                        Message dialog = new Message(singleSnapshot.getKey().toString(), user_sent, singleSnapshot.child("content").getValue().toString(), date);

                                        mediaPlayer.start();

                                        messagesAdapter.addToStart(dialog, true);
                                        // messages.add(dialog);


                                    }
                                }
                            else if (singleSnapshot.child("type").getValue().toString().equals("image")) {

                                        if (singleSnapshot.child("fromid").getValue().toString().equals(FirebaseAuth.getInstance().getCurrentUser().getUid())) {
                                            User user_sent = new User("0", con.child("name").getValue().toString(), con.child("avatar").getValue().toString(), Boolean.valueOf((Boolean) con.child("online").getValue()));
                                            Message dialog = new Message(singleSnapshot.getKey().toString(), user_sent, null, date);
                                            dialog.setImage(new Message.Image(singleSnapshot.child("content").getValue().toString()));
                                            messagesAdapter.addToStart(dialog, true);
                                            //  messages.add(dialog);
                                        } else {
                                            User user_sent = new User("1", con.child("name").getValue().toString(), con.child("avatar").getValue().toString(), Boolean.valueOf((Boolean) con.child("online").getValue()));
                                            Message dialog = new Message(singleSnapshot.getKey().toString(), user_sent, null, date);
                                            dialog.setImage(new Message.Image(singleSnapshot.child("content").getValue().toString()));

                                            mediaPlayer.start();

                                            messagesAdapter.addToStart(dialog, true);
                                            // messages.add(dialog);
                                        }

                                    }

                                else if (singleSnapshot.child("type").getValue().toString().equals("location")) {

                                    if (singleSnapshot.child("fromid").getValue().toString().equals(FirebaseAuth.getInstance().getCurrentUser().getUid())) {
                                        User user_sent = new User("0", con.child("name").getValue().toString(), con.child("avatar").getValue().toString(), Boolean.valueOf((Boolean) con.child("online").getValue()));
                                        Message dialog = new Message(singleSnapshot.getKey().toString(), user_sent, null, date);
                                        String content = singleSnapshot.child("content").getValue().toString();
                                        String[] Place = content.split(":");
                                        String Place_Url =  "http://maps.google.com/maps/api/staticmap?center=" +  Place[0].toString() + "," +  Place[1].toString() + "&zoom=15&size=600x400&sensor=false";
                                        dialog.setImage(new Message.Image(Place_Url));



                                        messagesAdapter.addToStart(dialog, true);
                                        //  messages.add(dialog);
                                    } else {
                                        User user_sent = new User("1", con.child("name").getValue().toString(), con.child("avatar").getValue().toString(), Boolean.valueOf((Boolean) con.child("online").getValue()));
                                        Message dialog = new Message(singleSnapshot.getKey().toString(), user_sent, null, date);
                                        String content = singleSnapshot.child("content").getValue().toString();
                                        String[] Place = content.split(":");
                                        String Place_Url =  "http://maps.google.com/maps/api/staticmap?center=" +  Place[0].toString() + "," +  Place[1].toString() + "&zoom=15&size=600x400&sensor=false";
                                        dialog.setImage(new Message.Image(Place_Url));

                                        mediaPlayer.start();

                                        messagesAdapter.addToStart(dialog, true);
                                        // messages.add(dialog);
                                    }

                                }
                                }

                             else if (lastid.equals(""))
                            {
                                lastid = singleSnapshot.getKey().toString();
                            }


                        }
                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });

                }
              //  getmessge();
          //  }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                      ///////
                System.out.println("The");
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

                System.out.println("The");
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {
                System.out.println("The");
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        };


        MessRef.orderByChild("timestamp").limitToLast(1).addChildEventListener(event);

    }

}
