//package com.chating.bo.chating.chat.custom.layout;
//
//import android.content.Context;
//import android.content.Intent;
//import android.os.Bundle;
//import android.util.Log;
//
//import com.chating.bo.chating.Enity.Dialog;
//import com.chating.bo.chating.R;
//import com.chating.bo.chating.chat.DemoDialogsActivity;
//import com.google.firebase.database.DataSnapshot;
//import com.google.firebase.database.DatabaseError;
//import com.google.firebase.database.DatabaseReference;
//import com.google.firebase.database.FirebaseDatabase;
//import com.google.firebase.database.ValueEventListener;
//import com.stfalcon.chatkit.dialogs.DialogsList;
//import com.stfalcon.chatkit.dialogs.DialogsListAdapter;
//
//import java.util.ArrayList;
//import java.util.List;
//
//
//public class CustomLayoutDialogsActivity extends DemoDialogsActivity {
//    private ArrayList<Dialog> Item = new ArrayList<Dialog>();
//    private DatabaseReference DialogRef;
//
//    public static void open(Context context) {
//        context.startActivity(new Intent(context, CustomLayoutDialogsActivity.class));
//    }
//
//    private DialogsList dialogsList;
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_custom_layo1ut_dialogs);
//
//        dialogsList = (DialogsList) findViewById(R.id.dialogsList);
//        initAdapter();
//        get_all_Dialog();
//    }
//
//    @Override
//    public void onDialogClick(Dialog dialog) {
//        CustomLayoutMessagesActivity.open(this);
//    }
//
//    private void initAdapter() {
//        super.dialogsAdapter = new DialogsListAdapter<>(R.layout.item_custom_dialog, super.imageLoader);
//        super.dialogsAdapter.setItems(Item);
//
//        super.dialogsAdapter.setOnDialogClickListener(this);
//        super.dialogsAdapter.setOnDialogLongClickListener(this);
//
//        dialogsList.setAdapter(super.dialogsAdapter);
//    }
//
//    private void get_all_Dialog(){
//        DialogRef = FirebaseDatabase.getInstance().getReference().child("User");
//        ValueEventListener postListener = new ValueEventListener() {
//            @Override
//            public void onDataChange(DataSnapshot dataSnapshot) {
//                // Get Post object and use the values to update the UI
//                Dialog dialog = dataSnapshot.getValue(Dialog.class);
//                Item.add(dialog);
//
//                // ...
//            }
//
//            @Override
//            public void onCancelled(DatabaseError databaseError) {
//                // Getting Post failed, log a message
//                Log.w(TAG, "loadPost:onCancelled", databaseError.toException());
//                // ...
//            }
//        };
//        DialogRef.addValueEventListener(postListener);
//
//    }
//}
