package com.chating.bo.chating.chat.Data.getlast;


import android.content.Context;

import com.chating.bo.chating.Enity.Dialog;

import java.util.ArrayList;


public class getlast_Presenter implements getlast_contract.Presenter,
        getlast_contract.OnGetLastgMessageListener {
    private getlast_contract.View mView;
    private getlast_Interator mgetlast_interactor;

    public getlast_Presenter(getlast_contract.View view) {
        this.mView = view;
        mgetlast_interactor = new getlast_Interator(this,this);

    }


    @Override
    public void getlast_Message(Context context) {
     mgetlast_interactor.getlast_Message_Firebase(context);
    }


    @Override
    public void onGetLastMessageSuccess(ArrayList<Dialog> Last_Message) {
            mView.onGetLastMessageSuccess(Last_Message);
    }

    @Override
    public void onGetLastMessageFailure(String message) {
        mView.onGetLastMessageFailure(message);

    }
}
