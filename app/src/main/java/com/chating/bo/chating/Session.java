package com.chating.bo.chating;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * Created by User on 6/20/2017.
 */

public class Session {
    private SharedPreferences prefs;

    public Session(Context cntx) {
        // TODO Auto-generated constructor stub
        prefs = PreferenceManager.getDefaultSharedPreferences(cntx);
    }

    public void set_userid(String usename) {
        prefs.edit().putString("usename", usename).commit();

    }

    public String get_userid() {
        String usename = prefs.getString("usename","");
        return usename;
    }


    public void set_chat_from(String from) {
        prefs.edit().putString("chat_from", from).commit();

    }

    public String get_chat_from() {
        String chat_from = prefs.getString("chat_from","");
        return chat_from;
    }

    public void set_chat_to(String to) {
        prefs.edit().putString("chat_to", to).commit();

    }

    public String get_chat_to() {
        String chat_to = prefs.getString("chat_to","");
        return chat_to;
    }

    public void set_chatID(String chatID) {
        prefs.edit().putString("chatID", chatID).commit();

    }

    public String get_chatID() {
        String chatID = prefs.getString("chatID","");
        return chatID;
    }
}
