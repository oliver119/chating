package com.chating.bo.chating.chat.Data.getlast;

import android.content.Context;

import com.chating.bo.chating.Enity.Dialog;

import java.util.ArrayList;

/**
 * Created by User on 6/29/2017.
 */

public interface getlast_contract {

    interface View {
        void onGetLastMessageSuccess(ArrayList<Dialog> Last_Message);

        void onGetLastMessageFailure(String message);

    }

    interface Presenter {

        void getlast_Message(Context context);
    }

    interface Interactor {

        void getlast_Message_Firebase(Context context);
    }

    interface OnGetLastgMessageListener {
        void onGetLastMessageSuccess(ArrayList<Dialog> Last_Message);

        void onGetLastMessageFailure(String message);
    }


}
