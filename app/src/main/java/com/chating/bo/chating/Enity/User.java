package com.chating.bo.chating.Enity;

import com.google.firebase.database.IgnoreExtraProperties;
import com.stfalcon.chatkit.commons.models.IUser;

/*
 * Created by troy379 on 04.04.17.
 */
@IgnoreExtraProperties
public class User implements IUser {

    private String id;
    private String name;
    private String avatar;

    public String getConversationID() {
        return ConversationID;
    }

    public void setConversationID(String conversationID) {
        ConversationID = conversationID;
    }

    private String ConversationID;

    public User(String id, String name, String avatar, String conversationID) {
        this.id = id;
        this.name = name;
        this.avatar = avatar;
        ConversationID = conversationID;
    }

    public User(String id, String name, String avatar) {
        this.id = id;
        this.name = name;
        this.avatar = avatar;
    }

    public User(String id, String name, String avatar, String conversationID, boolean online) {
        this.id = id;
        this.name = name;
        this.avatar = avatar;

        ConversationID = conversationID;
        this.online = online;

    }

    private boolean online;

    public User(String id, String name, String avatar, boolean online) {
        this.id = id;
        this.name = name;
        this.avatar = avatar;
        this.online = online;
    }
    public User(){};
    @Override
    public String getId() {
        return id;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getAvatar() {
        return avatar;
    }

    public boolean isOnline() {
        return online;
    }
}
