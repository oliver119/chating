package com.chating.bo.chating.chat.Data.getlast;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;

import com.chating.bo.chating.Enity.Dialog;
import com.chating.bo.chating.Enity.Message;
import com.chating.bo.chating.Enity.User;
import com.chating.bo.chating.MenuTabs_MainActivity;
import com.chating.bo.chating.R;
import com.chating.bo.chating.notification.Config;
import com.chating.bo.chating.utils.FormatUtils;
import com.chating.bo.chating.utils.NotificationUtils;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

/**
 * Created by User on 6/29/2017.
 */

public class getlast_Interator implements getlast_contract.Interactor {
    private getlast_contract.OnGetLastgMessageListener mOnLastMessageListener;
    ArrayList<Dialog> Item = new ArrayList<Dialog>();
    private static final int PRIORITY_HIGH = 5;

    public getlast_Interator(getlast_Presenter messageListener, getlast_contract.OnGetLastgMessageListener ongetlastMessageListener
    ) {
        this.mOnLastMessageListener = ongetlastMessageListener;

    }


    @Override
    public void getlast_Message_Firebase(final Context context) {

       DatabaseReference DialogRef = FirebaseDatabase.getInstance().getReference().child("User").getRef();

        ValueEventListener userListener = new ValueEventListener() {
            @Override
            public void onDataChange(final DataSnapshot dataSnapshotUser) {
              Item.clear();

                DataSnapshot conversa = dataSnapshotUser.child(FirebaseAuth.getInstance().getCurrentUser().getUid()).child("Conversations");

                for(DataSnapshot convergetid : conversa.getChildren()) {


                    final String converId = convergetid.child("location").getValue().toString();
                    DataSnapshot Userconver = dataSnapshotUser.child(convergetid.getKey());

                    String key = Userconver.getKey().toString();
                    String avatar = Userconver.child("avatar").getValue().toString();
                    String name = Userconver.child("name").getValue().toString();
                  //  getmess(key,name,avatar,converId);
                    unread(key,name,avatar,converId,context);

                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        };

        DialogRef.addValueEventListener(userListener);

    }

    private void getmess(final String key, final String name, final String avatar, String conID, final int Unread, final Context context){

        DatabaseReference MessRef = FirebaseDatabase.getInstance().getReference().child("Conversations").child(conID).getRef();



        ValueEventListener MessListener = new ValueEventListener() {
            @Override
            public void onDataChange(final DataSnapshot dataSnapshotMess)

            {


                //   mgetlast_Presenter.getlast_Message(converId,get_last,key,name,avatar);
                for (final DataSnapshot singleSnapshot : dataSnapshotMess.getChildren()) {
                    final User get_last = new User();



                    if (singleSnapshot.child("type").getValue().toString().equals("text")) {


                        Message last = new Message(singleSnapshot.getKey().toString(), get_last, singleSnapshot.child("content").getValue().toString(), FormatUtils.getDate((long) singleSnapshot.child("timestamp").getValue()));
                        ArrayList<User> user = new ArrayList<User>();
                        user.add(get_last);
                        Dialog dialog = new Dialog(key, name, avatar, user, last, Unread);
                        generateNotification(context,singleSnapshot.child("content").getValue().toString(),name );

                        Item.add(dialog);


                    } else if (singleSnapshot.child("type").getValue().toString().equals("image")) {
                        Message last = new Message(singleSnapshot.getKey().toString(), get_last, "<media>", FormatUtils.getDate((long) singleSnapshot.child("timestamp").getValue()));
                        ArrayList<User> user = new ArrayList<User>();
                        user.add(get_last);
                        Dialog dialog = new Dialog(key, name, avatar, user, last, Unread);
                        generateNotification(context,"<media>",name );


                        Item.add(dialog);

                    } else {
                        Message last = new Message(singleSnapshot.getKey().toString(), get_last, "<location>", FormatUtils.getDate((long) singleSnapshot.child("timestamp").getValue()));
                        ArrayList<User> user = new ArrayList<User>();
                        user.add(get_last);
                        Dialog dialog = new Dialog(key, name, avatar, user, last, Unread);

                        generateNotification(context,"<location>",name );

                        Item.add(dialog);
                    }

                }
                mOnLastMessageListener.onGetLastMessageSuccess(Item);

            }

            ////////


            @Override
            public void onCancelled(DatabaseError databaseError) {
                mOnLastMessageListener.onGetLastMessageFailure(databaseError.toString());

            }
        };
        MessRef.orderByChild("timestamp").limitToLast(1).addListenerForSingleValueEvent(MessListener);
    }

    private void unread(final String key, final String name, final String avatar, final String conID, final Context context){




        DatabaseReference UnreadRef = FirebaseDatabase.getInstance().getReference().child("Conversations").child(conID).getRef();
        UnreadRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {




                if(Item.size()!=0) {
                    for (int i = 0; i < Item.size(); i++) {
                        Dialog event = Item.get(i);
                        if (event.getId().equals(key)) {
                            Item.remove(i);
                        }

                    }
                }



                int unread=0;
                for(DataSnapshot childUnread : dataSnapshot.getChildren()){
                    if(childUnread.child("isRead").getValue().toString().equals("false")==true){
                        unread ++;
                    }

                }

                getmess(key,name,avatar,conID,unread,context);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

    private static void generateNotification(Context context, String message, String from){
        if (NotificationUtils.isAppIsInBackground(context)) {
            Intent notificationIntent = new Intent(context, MenuTabs_MainActivity.class);
            notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            PendingIntent intent = PendingIntent.getActivity(context, 0, notificationIntent, 0);

            NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context)
                    .setSmallIcon(R.mipmap.ic_app)
                    .setContentTitle(from)
                    .setContentIntent(intent)
                    .setPriority(PRIORITY_HIGH) //private static final PRIORITY_HIGH = 5;
                    .setContentText(message)
                    .setAutoCancel(true)
                    .setDefaults(Notification.DEFAULT_SOUND | Notification.DEFAULT_VIBRATE | Notification.DEFAULT_LIGHTS);
            NotificationManager mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            mNotificationManager.notify(0, mBuilder.build());
        }


    }
}
