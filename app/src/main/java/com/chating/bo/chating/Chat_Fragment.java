package com.chating.bo.chating;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.chating.bo.chating.chat.Data.Add_Dataset;
import com.chating.bo.chating.chat.Data.getlast.getlast_Presenter;
import com.chating.bo.chating.chat.Data.getlast.getlast_contract;
import com.chating.bo.chating.Enity.Dialog;
import com.chating.bo.chating.Enity.Message;
import com.chating.bo.chating.Enity.User;
import com.chating.bo.chating.chat.DemoDialogsActivity;

import com.chating.bo.chating.chat.custom.layout.CustomLayoutMessagesActivity;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.squareup.picasso.Picasso;
import com.stfalcon.chatkit.commons.ImageLoader;
import com.stfalcon.chatkit.dialogs.DialogsList;
import com.stfalcon.chatkit.dialogs.DialogsListAdapter;

import java.util.ArrayList;
import java.util.Date;

public class Chat_Fragment extends DemoDialogsActivity implements getlast_contract.View{
    private static final String TAG = "MainActivity";

    private ArrayList<Dialog> Item = new ArrayList<Dialog>();
    private ArrayList<User> user = new ArrayList<User>();
    private DatabaseReference DialogRef,UserRef,MessRef;
    private Add_Dataset Add_Database = new Add_Dataset();
private Session session;

    private getlast_Presenter mgetlast_Presenter;


    private KProgressHUD hud;


    public static Chat_Fragment newInstance(String type) {
        Chat_Fragment myFragment = new Chat_Fragment();


        Bundle args = new Bundle();
        args.putString("type", type);
        myFragment.setArguments(args);

        return myFragment;
    }


    public static void open(Context context) {
        //        i.putExtra("Conver_id", Conver_id);
        context.startActivity(new Intent(context, Chat_Fragment.class));
    }

    private DialogsList dialogsList;
    private String type="";


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_custom_layout_dialogs, container, false);
intit_View();
        dialogsList = (DialogsList) view.findViewById(R.id.dialogsList);

        type = getArguments().getString("type");

        if(type.equals("all")){
            get_all_User();
        }
        else {
            get_all_Recent();
        }



        return view;
    }

    @Override
    public void onDialogClick(Dialog dialog) {
      String  Conver_id = dialog.getId().toString()+"_"+FirebaseAuth.getInstance().getCurrentUser().getUid();
        session.set_chatID(Conver_id);


        Check_Conversation(dialog.getId());
        String  Revcen_id = dialog.getId();

        session.set_chat_to(Revcen_id);

        CustomLayoutMessagesActivity.open(getActivity());

   }

    private void initAdapter(ArrayList<Dialog> Dialog_Item) {
        imageLoader = new ImageLoader() {
            @Override
            public void loadImage(ImageView imageView, String url) {
                Picasso.with(getActivity())
                        .load(url)
                        .into(imageView);

            }
        };
        super.dialogsAdapter = new DialogsListAdapter<>(R.layout.item_custom_dialog, imageLoader);
        super.dialogsAdapter.setItems(Dialog_Item);

        super.dialogsAdapter.setOnDialogClickListener(this);
        super.dialogsAdapter.setOnDialogLongClickListener(this);

        dialogsList.setAdapter(super.dialogsAdapter);
    }


    private void get_all_User(){
        UserRef = FirebaseDatabase.getInstance().getReference().child("User").getRef();
        ValueEventListener userListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // Get Post object and use the values to update the UI
              Item.clear();
                hud.show();
                Date date = new Date();

              //  DataSnapshot con = dataSnapshot.child("9NgL5tKd7pPkUFLuItnnfK21aef1");

             //   User get_last =  new User(con.getKey().toString(),con.child("name").getValue().toString(),con.child("avatar").getValue().toString(),Boolean.valueOf((Boolean)con.child("online").getValue()));


                User get_last =  new User();
                user.add(get_last);

                Message last_fake = new Message("1221564445",get_last,"",null);

                for(DataSnapshot singleSnapshot : dataSnapshot.getChildren()) {
                    Dialog dialog = new Dialog(singleSnapshot.getKey().toString(), singleSnapshot.child("name").getValue().toString(), singleSnapshot.child("avatar").getValue().toString(), user, last_fake,0);
                    Item.add(dialog);
                }

                initAdapter(Item);
                hud.dismiss();


            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                // Getting Post failed, log a message
                Log.w(TAG, "loadPost:onCancelled", databaseError.toException());
                // ...
            }
        };
        UserRef.addValueEventListener(userListener);

    }
    private void intit_View(){
        session = new Session(getActivity());
        mgetlast_Presenter = new getlast_Presenter(this);

        hud = KProgressHUD.create(getActivity())
                .setStyle(KProgressHUD.Style.ANNULAR_DETERMINATE)
                .setLabel("Please wait")
                .setMaxProgress(100);

        hud.setProgress(90);
    }

    private void Check_Conversation(final String idcheck){
        UserRef = FirebaseDatabase.getInstance().getReference().child("User").child(FirebaseAuth.getInstance().getCurrentUser().getUid()).child("Conversations");
        ValueEventListener userListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {


          if(dataSnapshot.child(idcheck).exists()==false){
              Add_Database.Add_Conversation(idcheck,FirebaseAuth.getInstance().getCurrentUser().getUid());
          }
          else {
             String Conver_id = dataSnapshot.child(idcheck).child("location").getValue().toString();
              session.set_chatID(Conver_id);
          }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                // Getting Post failed, log a message
                Log.w(TAG, "loadPost:onCancelled", databaseError.toException());
                // ...
            }
        };
        UserRef.addListenerForSingleValueEvent(userListener);

    }


    private void get_all_Recent(){
        hud.show();

                    mgetlast_Presenter.getlast_Message(getActivity());


    }



    @Override
    public void onGetLastMessageSuccess(ArrayList<Dialog> Last_Message) {


        hud.dismiss();

            initAdapter(Last_Message);

    }

    @Override
    public void onGetLastMessageFailure(String message) {
        System.out.println(message);

    }
}
