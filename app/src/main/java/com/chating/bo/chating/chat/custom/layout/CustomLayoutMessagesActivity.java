package com.chating.bo.chating.chat.custom.layout;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.FileProvider;
import android.text.format.DateFormat;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.chating.bo.chating.BuildConfig;

import com.chating.bo.chating.Session;
import com.chating.bo.chating.chat.Data.Add_Dataset;
import com.chating.bo.chating.Enity.Message;
import com.chating.bo.chating.Enity.User;
import com.chating.bo.chating.Chat_Fragment;
import com.chating.bo.chating.MenuTabs_MainActivity;
import com.chating.bo.chating.R;
import com.chating.bo.chating.chat.DemoMessagesActivity;
import com.chating.bo.chating.chat.FullScreenImageActivity;
import com.chating.bo.chating.utils.AppUtils;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.google.gson.JsonElement;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.roughike.bottombar.BottomBar;
import com.roughike.bottombar.OnTabReselectListener;
import com.roughike.bottombar.OnTabSelectListener;
import com.stfalcon.chatkit.messages.MessageHolders;
import com.stfalcon.chatkit.messages.MessageInput;
import com.stfalcon.chatkit.messages.MessagesList;
import com.stfalcon.chatkit.messages.MessagesListAdapter;

import java.io.File;
import java.util.Date;
import java.util.Map;
import java.util.UUID;

import ai.api.AIServiceException;

import ai.api.android.AIConfiguration;
import ai.api.android.AIDataService;
import ai.api.android.AIService;
import ai.api.model.AIError;
import ai.api.model.AIRequest;
import ai.api.model.AIResponse;
import ai.api.model.Result;


public class CustomLayoutMessagesActivity extends DemoMessagesActivity
        implements MessagesListAdapter.OnMessageLongClickListener<Message>, GoogleApiClient.OnConnectionFailedListener, MessagesListAdapter.OnMessageClickListener<Message>,
        MessageInput.InputListener,
        MessageInput.AttachmentsListener,ai.api.AIListener {
    private DatabaseReference MessRef, UserRef;

    String avatar_url;
    FirebaseStorage storage = FirebaseStorage.getInstance();
    StorageReference storageRef = storage.getReferenceFromUrl("gs://chating-94295.appspot.com/");
    BottomBar bottomBar;
    Uri filePath;
    private static final int PICK_IMAGE_REQUEST = 1;
    private static final int IMAGE_CAMERA_REQUEST = 2;
    private static final int PLACE_PICKER_REQUEST = 3;

    private static final int REQUEST_EXTERNAL_STORAGE = 4;
    private static String[] PERMISSIONS_STORAGE = {
            android.Manifest.permission.READ_EXTERNAL_STORAGE,
            android.Manifest.permission.WRITE_EXTERNAL_STORAGE
    };

    private KProgressHUD hud;
    private GoogleApiClient mGoogleApiClient;

    private ImageView back_btn;
    private TextView detail_tv;



    //File
    private File filePathImageCamera;

///api.ai
    public AIService aiService;
    AIConfiguration aiConfiguration;
    private AIDataService aiDataService;


    ////session


private Session session;

    public static void open(Context context) {

        Intent i = new Intent(context, CustomLayoutMessagesActivity.class);



        context.startActivity(i);


    }

    private MessagesList messagesList;

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        Add_Dataset dataset = new Add_Dataset();


        dataset.readed_back(session.get_chatID());



        finish();



        Intent I = new Intent(this,MenuTabs_MainActivity.class);
        startActivity(I);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_custom_layout_messages);
        initView();
        messagesList = (MessagesList) findViewById(R.id.messagesList);
        initAdapter();

        MessageInput input = (MessageInput) findViewById(R.id.input);
        input.setInputListener(this);
        input.setAttachmentsListener(this);

    }
    private void initView(){

        session = new Session(this);

        bottomBar = (BottomBar) findViewById(R.id.bottomBar);
        bottomBar.setVisibility(View.GONE);

        hud = KProgressHUD.create(CustomLayoutMessagesActivity.this)
                .setStyle(KProgressHUD.Style.ANNULAR_DETERMINATE)
                .setLabel("Please wait")
                .setMaxProgress(100);

        hud.setProgress(90);

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API)
                .build();


        ////api.ai

        aiConfiguration = new AIConfiguration(AppUtils.API_Token,
                AIConfiguration.SupportedLanguages.English,
                AIConfiguration.RecognitionEngine.System);

        aiDataService = new AIDataService(this,aiConfiguration);

        back_btn  = (ImageView) findViewById(R.id.iv_back);
        back_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
finish();
            }
        });

        detail_tv  = (TextView) findViewById(R.id.tv_title);

        getNameDetalis();


      // aiService.setListener(this);


    }
private void getNameDetalis(){
    Session session = new Session(this);



    DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child("User").child(session.get_chat_to()).getRef();
    ref.addListenerForSingleValueEvent(new ValueEventListener() {
        @Override
        public void onDataChange(DataSnapshot dataSnapshot) {

            detail_tv.setText(  dataSnapshot.child("name").getValue().toString());
        }

        @Override
        public void onCancelled(DatabaseError databaseError) {

        }
    });

}
    @Override
    public boolean onSubmit(CharSequence input) {

     // messagesAdapter.addToStart(message, true);
        sent_message_Firebase(input.toString(),"text");
        if(session.get_chat_to().equals("Autobot123434")){
            bot_request(input.toString());
          //  aiService.startListening();

        }

       return true;
    }

    @Override
    public void onAddAttachments() {
        Toast.makeText(this,"hello",Toast.LENGTH_SHORT).show();
      //  messagesAdapter.addToStart(MessagesFixtures.getImageMessage(), true);

bottomBar.setVisibility(View.VISIBLE);


        bottomBar.setOnTabSelectListener(new OnTabSelectListener() {
            @Override
            public void onTabSelected(@IdRes int tabId) {
                switch (tabId) {
                    case R.id.tab_recents:
                        Toast.makeText(CustomLayoutMessagesActivity.this, "Click to cancel", Toast.LENGTH_SHORT).show();
                        break;

                    case R.id.tab_image:
                        Intent intent = new Intent();
                        intent.setType("image/*");
                        intent.setAction(Intent.ACTION_PICK);

                        startActivityForResult(Intent.createChooser(intent,"Select Image"), PICK_IMAGE_REQUEST);

                        break;
                    case R.id.tab_location:
                        locationPlacesIntent();

                        break;

                    case R.id.tab_camera:
                        verifyStoragePermissions();
                       // photoCameraIntent();

                        break;

                }
            }
        });

        bottomBar.setOnTabReselectListener(new OnTabReselectListener() {
            @Override
            public void onTabReSelected(@IdRes int tabId) {
                switch (tabId) {
                    case R.id.tab_recents:
                    bottomBar.setVisibility(View.GONE);
                        break;

                    case R.id.tab_image:
                        Intent intent = new Intent();
                        intent.setType("image/*");
                        intent.setAction(Intent.ACTION_PICK);

                        startActivityForResult(Intent.createChooser(intent,"Select Image"), PICK_IMAGE_REQUEST);

                        break;
                    case R.id.tab_location:
                        locationPlacesIntent();

                        break;

                    case R.id.tab_camera:
                        verifyStoragePermissions();
                        photoCameraIntent();

                        break;


                }
            }
        });

    }
    private void upload_avatar(){
        if(filePath != null) {
            hud.show();
          //  Date aa = new Date();

            String uniqueID_image = UUID.randomUUID().toString();
            final StorageReference childRef = storageRef.child(FirebaseAuth.getInstance().getCurrentUser().getUid()+"/"+uniqueID_image+".jpg");

            //uploading the image
            UploadTask uploadTask = childRef.putFile(filePath);

            uploadTask.addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(final UploadTask.TaskSnapshot taskSnapshot) {
                    hud.dismiss();
                    Toast.makeText(CustomLayoutMessagesActivity.this, "Upload successful", Toast.LENGTH_SHORT).show();
                    ///// get url upload
                    childRef.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                        @Override
                        public void onSuccess(Uri uri) {
                            // Got the download URL for 'users/me/profile.png'
                            Uri downloadUri = taskSnapshot.getMetadata().getDownloadUrl();
                            avatar_url = downloadUri.toString(); /// The string(file link) that you need


                         //   add image();

                            sent_message_Firebase(avatar_url,"image");

                        }
                    }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception exception) {
                            // Handle any errors
                        }
                    });



                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    hud.dismiss();
                    Toast.makeText(CustomLayoutMessagesActivity.this, "Upload Failed -> " + e, Toast.LENGTH_SHORT).show();
                }
            });
        }
        else {
            Toast.makeText(CustomLayoutMessagesActivity.this, "Select an image", Toast.LENGTH_SHORT).show();
        }



    }
    private void locationPlacesIntent(){
        try {
            PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
            startActivityForResult(builder.build(this), PLACE_PICKER_REQUEST);
        } catch (GooglePlayServicesRepairableException | GooglePlayServicesNotAvailableException e) {
            e.printStackTrace();
        }
    }

    private void photoCameraIntent(){
        String nomeFoto = DateFormat.format("yyyy-MM-dd_hhmmss", new Date()).toString();
        filePathImageCamera = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), nomeFoto+"camera.jpg");
        Intent it = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
         filePath = FileProvider.getUriForFile(CustomLayoutMessagesActivity.this,
                BuildConfig.APPLICATION_ID + ".provider",
                filePathImageCamera);
        it.putExtra(MediaStore.EXTRA_OUTPUT,filePath);
        startActivityForResult(it, IMAGE_CAMERA_REQUEST);

    }
private void sent_message_Firebase(String contend, String type){

    User user = new User("0",FirebaseAuth.getInstance().getCurrentUser().getDisplayName(),FirebaseAuth.getInstance().getCurrentUser().getPhotoUrl().toString(),false);
    String uniqueID = UUID.randomUUID().toString(); //// unique id
    // Message message  =new Message(uniqueID,user,input.toString());
    Add_Dataset addmess  = new Add_Dataset();
    addmess.Add_Message(contend,user, session.get_chatID(),uniqueID,type);
}


    @Override
    public void onMessageLongClick(Message message) {
     //   AppUtils.showToast(this, message.getText().toLowerCase(), false);

        getclick_dataFirebase(message.getId());
    }


    private void initAdapter() {
        MessageHolders holdersConfig = new MessageHolders()
                .setIncomingTextLayout(R.layout.item_custom_incoming_text_message)
                .setOutcomingTextLayout(R.layout.item_custom_outcoming_text_message)
                .setIncomingImageLayout(R.layout.item_custom_incoming_image_message)
                .setOutcomingImageLayout(R.layout.item_custom_outcoming_image_message);

        super.messagesAdapter = new MessagesListAdapter<>(super.senderId, holdersConfig, super.imageLoader);
        super.messagesAdapter.setOnMessageLongClickListener(this);
        super.messagesAdapter.setLoadMoreListener(this);
        messagesList.setAdapter(super.messagesAdapter);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {
            filePath = data.getData();

            try {
                //getting image from gallery
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), filePath);

                ///upload
                upload_avatar();


            } catch (Exception e) {
                e.printStackTrace();

            }
        } else if (requestCode == PLACE_PICKER_REQUEST) {
            if (resultCode == RESULT_OK) {
                Place place = PlacePicker.getPlace(this, data);
                if (place != null) {
                    LatLng latLng = place.getLatLng();

                    // add loaction
                    sent_message_Firebase(latLng.latitude + ":" + latLng.longitude, "location");


                } else {
                    //PLACE IS NULL
                }
            }

        } else if (requestCode == IMAGE_CAMERA_REQUEST) {
            if (resultCode == RESULT_OK) {

                if (filePathImageCamera != null && filePathImageCamera.exists()){
                    upload_avatar();
                }else{
                    //IS NULL
                }
              //  filePath=  data.getData();


            }


        }
    }

    private void bot_request(String text){
        final AIRequest aiRequest = new AIRequest();
        aiRequest.setQuery(text);

        new AsyncTask<AIRequest, Void, AIResponse>() {
            @Override
            protected AIResponse doInBackground(AIRequest... requests) {
                final AIRequest request = requests[0];
                try {
                    final AIResponse response = aiDataService.request(aiRequest);
                    return response;
                } catch (AIServiceException e) {

                    ////
                }
                return null;
            }
            @Override
            protected void onPostExecute(AIResponse aiResponse) {
                if (aiResponse != null) {
                    Result result = aiResponse.getResult();

                    // Get parameters
                    String parameterString = "";


                    parameterString=  result.getFulfillment().getSpeech();
                    sent_message_Firebase_bot(parameterString,"text");
                }
            }
        }.execute(aiRequest);


    }



    private void sent_message_Firebase_bot(String contend, String type){

        User user = new User("0","bot","https://marketplace.canva.com/MAB6v9eTAHs/1/thumbnail/canva-robot-avatar-MAB6v9eTAHs.png",false);
        String uniqueID = UUID.randomUUID().toString(); //// unique id
        // Message message  =new Message(uniqueID,user,input.toString());
        Add_Dataset addmess  = new Add_Dataset();
        addmess.Add_Message_Bot(contend,user, session.get_chatID(),uniqueID,type);
    }


    public void verifyStoragePermissions() {
        // Check if we have write permission
        int permission = ActivityCompat.checkSelfPermission(CustomLayoutMessagesActivity.this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if (permission != PackageManager.PERMISSION_GRANTED) {
            // We don't have permission so prompt the user
            ActivityCompat.requestPermissions(
                    CustomLayoutMessagesActivity.this,
                    PERMISSIONS_STORAGE,
                    REQUEST_EXTERNAL_STORAGE
            );
        }else{
            // we already have permission, lets go ahead and call camera intent
            photoCameraIntent();
        }
    }

    private void getclick_dataFirebase(String message_key){

    DatabaseReference ClickRef =  FirebaseDatabase.getInstance().getReference().child("Conversations").child(session.get_chatID()).child(message_key).getRef();

    ClickRef.addListenerForSingleValueEvent(new ValueEventListener() {
        @Override
        public void onDataChange(DataSnapshot dataSnapshot) {
            String type= dataSnapshot.child("type").getValue().toString();
            if(type.equals("text")){

           Toast.makeText(CustomLayoutMessagesActivity.this, "text", Toast.LENGTH_SHORT).show();
            }
            else if(type.equals("image")){
                Toast.makeText(CustomLayoutMessagesActivity.this, "image", Toast.LENGTH_SHORT).show();

                Intent intent_image = new Intent(CustomLayoutMessagesActivity.this,FullScreenImageActivity.class);
                intent_image.putExtra("nameUser",FirebaseAuth.getInstance().getCurrentUser().getDisplayName());
                intent_image.putExtra("urlPhotoUser",FirebaseAuth.getInstance().getCurrentUser().getPhotoUrl());
                intent_image.putExtra("urlPhotoClick",dataSnapshot.child("content").getValue().toString());
                startActivity(intent_image);
            }
            else {
                Toast.makeText(CustomLayoutMessagesActivity.this, "location", Toast.LENGTH_SHORT).show();

                String content = dataSnapshot.child("content").getValue().toString() ;
                String[] Place = content.split(":");
                String latitude = Place[0].toString();
                String longitude = Place[1].toString();

                String uri = String.format("geo:%s,%s?z=17&q=%s,%s", latitude,longitude,latitude,longitude);
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
                startActivity(intent);
            }
        }

        @Override
        public void onCancelled(DatabaseError databaseError) {

        }
    });


}
    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
      //  Log.d(TAG, "onConnectionFailed:" + connectionResult);
       // Util.initToast(this,"Google Play Services error.");
        AppUtils.showToast(this, R.string.googe_error, false);
    }


    @Override
    public void onMessageClick(Message message) {
        AppUtils.showToast(this, R.string.googe_error, false);
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        switch (requestCode){
            case REQUEST_EXTERNAL_STORAGE:
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted
                    photoCameraIntent();
                }
                break;
        }
    }

    @Override
    public void onError(AIError error) {

    }

    @Override
    public void onAudioLevel(float level) {

    }

    @Override
    public void onListeningStarted() {
Toast.makeText(this,"bat dau",Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onListeningCanceled() {
        Toast.makeText(this,"huy",Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onListeningFinished() {
        Toast.makeText(this,"kethuc",Toast.LENGTH_SHORT).show();
    }
}
