package com.chating.bo.chating.utils;

import android.content.Context;
import android.support.annotation.StringRes;
import android.widget.Toast;

/*
 * Created by troy379 on 04.04.17.
 */
public class AppUtils {

    public static final String API_Token = "1ea701f9d3a2441393415a0994b8a173";


    public static void showToast(Context context, @StringRes int text, boolean isLong) {
        showToast(context, context.getString(text), isLong);
    }

    public static void showToast(Context context, String text, boolean isLong) {
        Toast.makeText(context, text, isLong ? Toast.LENGTH_LONG : Toast.LENGTH_SHORT).show();
    }
}