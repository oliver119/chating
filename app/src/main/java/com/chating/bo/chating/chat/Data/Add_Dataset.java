package com.chating.bo.chating.chat.Data;

import com.chating.bo.chating.Enity.User;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ServerValue;
import com.google.firebase.database.ValueEventListener;

import java.util.HashMap;
import java.util.Map;


/**
 * Created by User on 6/16/2017.
 */

public class Add_Dataset {
    private DatabaseReference mDatabase,UserRef;


    DatabaseReference database = FirebaseDatabase.getInstance().getReference();


    public void Add_New_User(String userId, String name,String Avatar_URL, Boolean Online) {
        database = FirebaseDatabase.getInstance().getReference();
        User user = new User(userId,name,Avatar_URL,Online);

        database.child("User").child(userId).setValue(user);

    }


    public void Add_Conversation(String userIde_chat,String recve) {
        database = FirebaseDatabase.getInstance().getReference();

        String conver_id=userIde_chat+"_"+recve;


        database.child("User").child(FirebaseAuth.getInstance().getCurrentUser().getUid()).child("Conversations").child(userIde_chat).child("location").setValue(conver_id);
        database.child("User").child(userIde_chat).child("Conversations").child(recve).child("location").setValue(conver_id);
       // database.child("Conversations").child(conver_id);

    }

    public void Add_Message(String Text,User user, String Conver_ID, String messsId,String type ) {
        database = FirebaseDatabase.getInstance().getReference();
        Map data = new HashMap();

        data.put("timestamp", ServerValue.TIMESTAMP);
        data.put("content", Text);
        data.put("fromid", FirebaseAuth.getInstance().getCurrentUser().getUid());

        String[] ID = Conver_ID.split("_");

        data.put("toid", ID[0]);

       // data.put("Recenid", FirebaseAuth.getInstance().getCurrentUser().getUid());
        data.put("isRead", false);
        data.put("type", type);



        database.child("Conversations").child(Conver_ID).child(messsId).setValue(data);

     //   database.child("Conversations").orderByChild("timestamp"). limitToLast(20);



    }

    public void Add_Message_Bot(String Text,User user, String Conver_ID, String messsId,String type ) {
        database = FirebaseDatabase.getInstance().getReference();
        Map data = new HashMap();

        data.put("timestamp", ServerValue.TIMESTAMP);
        data.put("content", Text);
        data.put("fromid", "Autobot123434");

        String[] ID = Conver_ID.split("_");

        data.put("toid",  FirebaseAuth.getInstance().getCurrentUser().getUid());

        // data.put("Recenid", FirebaseAuth.getInstance().getCurrentUser().getUid());
        data.put("isRead", false);
        data.put("type", type);



        database.child("Conversations").child(Conver_ID).child(messsId).setValue(data);

        //   database.child("Conversations").orderByChild("timestamp"). limitToLast(20);



    }


    public void readed(final String key, final String id) {
        database = FirebaseDatabase.getInstance().getReference();


        DatabaseReference Check = database.child("Conversations").child(key).getRef();

        Check.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()==false){

                }
                else {
                    database.child("Conversations").child(key).child(id).child("isRead").setValue(true);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });




    }
    public void readed_back (final String key) {
        database = FirebaseDatabase.getInstance().getReference();


        DatabaseReference Check = database.child("Conversations").child(key).getRef();

        Check.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()==false){

                }
                else {
                    for (DataSnapshot child : dataSnapshot.getChildren() )

                    database.child("Conversations").child(key).child(child.getKey().toString()).child("isRead").setValue(true);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


    }


    public  void Update_profile_User(String Userid, String name, String avatar_url) {

        //String key = mDatabase.child("User").child(Userid).push().getKey();
        User User = new User(Userid,name,avatar_url);



        database.child("User").child(Userid).child("name").setValue(name);

        database.child("User").child(Userid).child("avatar").setValue(avatar_url);






    }



}
