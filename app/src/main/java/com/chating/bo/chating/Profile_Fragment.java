package com.chating.bo.chating;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.chating.bo.chating.chat.Data.Add_Dataset;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import com.squareup.picasso.Picasso;

import java.util.Map;

import ai.api.AIListener;
import de.hdodenhof.circleimageview.CircleImageView;

import static android.app.Activity.RESULT_OK;
import static android.content.ContentValues.TAG;

/**
 * Created by User on 7/6/2017.
 */

public class Profile_Fragment extends Fragment implements  View.OnClickListener{

    private CircleImageView avatar;

    private EditText phone, name;


    private Button Submit;
    private TextView avatar_picker;

    private DatabaseReference UserRef;
    private Add_Dataset update_data;
    FirebaseDatabase database = FirebaseDatabase.getInstance();
    ////// Strorage
    FirebaseStorage storage = FirebaseStorage.getInstance();
    StorageReference storageRef = storage.getReferenceFromUrl("gs://chating-94295.appspot.com");

    int PLACE_PICKER_REQUEST = 1;
    int PICK_IMAGE_REQUEST = 111;
    Uri filePath;
    ProgressDialog pd;

    //////get map

    LocationManager locationManager;
    LocationListener locationListener;

    ///// set data
    private Double along;
    private Double la;
    private String avatar_url;
    private String Address;
    private String Phone;
    private String Name;



    ///
    private GoogleApiClient mGoogleApiClient;



 
    private Context mContext;


    public static Profile_Fragment newInstance() {
        Profile_Fragment fragment = new Profile_Fragment();
        return fragment;
    }



    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.activity_register, container, false);


        mContext = getActivity();

        initView(v);

        get_data_User();
        //// get map

        pd = new ProgressDialog(mContext);
        pd.setMessage("Uploading....");


        mGoogleApiClient = new GoogleApiClient.Builder(mContext)
               // .enableAutoManage(this, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API)
                .build();

        return v;
    }

    private void initView(View v){



        update_data=new Add_Dataset();
        avatar=(CircleImageView) v.findViewById(R.id.iv_profile);
        phone=(EditText) v.findViewById(R.id.phone_user);
        name=(EditText) v.findViewById(R.id.name_user);

        avatar_picker=(TextView) v.findViewById(R.id.avatar_picker);
        avatar_picker.setOnClickListener(this);

        Submit = (Button) v.findViewById(R.id.btn_done);
        Submit.setOnClickListener(this);






    }
    public void get_data_User() {
        UserRef = database.getReference().child("User").child(FirebaseAuth.getInstance().getCurrentUser().getUid());
        ValueEventListener postListener = new ValueEventListener() {
            ///
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                User(dataSnapshot);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.w(TAG, "Failed to read value.", databaseError.toException());

            }
        };
        UserRef.addListenerForSingleValueEvent(postListener);


    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        try {

            locationManager.removeUpdates(locationListener);


            super.onDestroy();

        }

        catch (Exception e) {

        }

    }

    public   void User(DataSnapshot users){

        //iterate through each user, ignoring their UID



        //Get user map
        Map singleUser = (Map) users.getValue();

        //Get user field and append to list

      //  phone.setText( singleUser.get("phone").toString());
        name.setText( singleUser.get("name").toString());
        Picasso.with(mContext).load( singleUser.get("avatar").toString()).into(avatar);

       // Phone=  singleUser.get("phone").toString();
        Name=  singleUser.get("name").toString();
      //  Address = singleUser.get("address").toString();
        avatar_url = singleUser.get("avatar").toString();


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {



            case R.id.avatar_picker:

                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_PICK);

                startActivityForResult(Intent.createChooser(intent,"Select Image"), PICK_IMAGE_REQUEST);
                break;

            case R.id.btn_done:

                AlertDialog.Builder builder = new AlertDialog.Builder(mContext);

                builder.setTitle("Confirm");
                builder.setMessage("Are you sure ?");

                builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {

                        Phone = phone.getText().toString();
                        Name = name.getText().toString();
                        update_data.Update_profile_User(FirebaseAuth.getInstance().getCurrentUser().getUid().toString(), Name,  avatar_url);

                        dialog.dismiss();

                        // finish();


                    }
                });

                builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        // Do nothing
                        dialog.dismiss();
                    }
                });

                AlertDialog alert = builder.create();
                alert.show();


                break;




        }
    }

    private void upload_avatar(){
        if(filePath != null) {
            pd.show();

            final StorageReference childRef = storageRef.child("avatar/"+ FirebaseAuth.getInstance().getCurrentUser().getUid()+".jpg");

            //uploading the image
            UploadTask uploadTask = childRef.putFile(filePath);

            uploadTask.addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(final UploadTask.TaskSnapshot taskSnapshot) {
                    pd.dismiss();
                    Toast.makeText(mContext, "Upload successful", Toast.LENGTH_SHORT).show();
                    ///// get url upload
                    childRef.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                        @Override
                        public void onSuccess(Uri uri) {
                            // Got the download URL for 'users/me/profile.png'
                            Uri downloadUri = taskSnapshot.getMetadata().getDownloadUrl();
                            avatar_url = downloadUri.toString(); /// The string(file link) that you need
                        }
                    }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception exception) {
                            // Handle any errors
                        }
                    });



                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    pd.dismiss();
                    Toast.makeText(mContext, "Upload Failed -> " + e, Toast.LENGTH_SHORT).show();
                }
            });
        }
        else {
            Toast.makeText(mContext, "Select an image", Toast.LENGTH_SHORT).show();
        }



    }




    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {
            filePath = data.getData();

            try {
                //getting image from gallery
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(mContext.getContentResolver(), filePath);

                //Setting image to ImageView
                avatar.setImageBitmap(bitmap);

                ///upload
                upload_avatar();

            } catch (Exception e) {
                e.printStackTrace();

            }


        }



    }



}
